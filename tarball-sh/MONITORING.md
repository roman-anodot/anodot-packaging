# Anodot Prometheus Monitoring Manual Setup

The process below briefly outlines the steps to manually install Prometheus
monitoring into an Anodot on-prem environment. 

*This is only intended for DevOps exploratory use. The actual install for on-prem releases will be conducted using a helmfile script with all the packages and deployment files pre-loaded.*

## Installation

1. Download prometheus monitoring installation tar file to system
   
   ```bash
   wget https://anodot-packages.s3.amazonaws.com/tarballs/prometheus_monitor.tar
   ```

2. Untar the file & cd to directory
   
   ```bash
   tar -xvf prometheus_monitor.tar
   # this creates a Monitor directory
   cd Monitor
   ```

3. Load Docker images for monitoring [(see image list below)](#required-image-repositories-and-tags)
   
   ```bash
   docker load < prometheus_images.tgz
   ```

4. Copy setup tar files to anodot-kubespray container
   
   ```bash
   anodocker cp prometheus_monitoring.tgz anodot-kubespray:.
   anodocker cp helm_repo.tgz anodot-kubespray:.
   anodocker cp monitoring.tgz anodot-kubespray:.
   anodocker cp helmfile_linux_386 anodot-kubespray:/usr/local/bin/helmfile
   ```

5. Exec into anodot-kubespray container
   
   ```bash
   anodocker exec -it anodot-kubespray bash
   ```

6. Untar the setup tar files within the anodot-kubespray container
   
   ```bash
   tar -xzvf /helm_repo.tgz
   tar -zxvf /monitoring.tgz
   tar -xzvf /prometheus_monitoring.tgz
   ```

7. Create persistent storage and storage classes for Victoria Metrics and Grafana
   
   ```bash
   cd /root/anodot-kubespray
   ./pv_monitoring.sh
   ```

8. Run helmfile to deploy prometheus monitoring configuration into monitoring namespace
   
   ```bash
   cd /root/prometheus_monitoring
   helmfile -i -e onprem apply
   kubectl get pods -n monitoring
   exit
   ```

9. Update Kubelet config's readonly port to 10255 and restart service
   
   ```bash
   # execute from Monitor directory on system not within anodot-kubespray
   sed -i '/readOnlyPort:/s/$/ 10255/' /etc/kubernetes/kublet-config.yaml
   systemctl status kubeket
   systemctl restart kubelet
   systemctl status kubelet
   ```

10. **Extra**: Install Cassandra JMX metrics exporter - **Requires Cassandra Restart**
    
    ```bash
    # execute from Monitor directory on system not within anodot-kubespray
    tar xvf cassandra.tgz
    docker load < jmx-cassandra-exporter.tar
    # updates cassandra sts and service, adds a sidecar container 
    kubectl apply -f cass.yaml
    ```

## Validation

Checks to make sure metrics appear and data points are updated in monitoring based services

#### Prometheus

```bash
kubectl port-forward -n monitoring svc/prometheus-prometheus --address 0.0.0.0 9090:9090
```

[  http://HOSTNAMEorIP:9090]()

- Prometheus Targets
  [http://HOSTNAMEorIP:9090/targets]()
- Prometheus Service Discovery
  [http://HOSTNAMEorIP:9090/service-discovery]()
- Prometheus Graph Search:
  - elastic
  - node_disk
  - kube_pod
  - container

#### Grafana

  [http://HOSTNAMEorIP:8000]()

```bash
  kubectl port-forward -n monitoring svc/grafana --address 0.0.0.0 8000:80
```

- review elasticsearch dashboard
- review victoria metrics dashboard

#### Anodot

  [http://HOSTNAMEorIP]()

- Metric Explorer Search:
  - elastic*
  - mongo*
  - node*
  - container*

## Required Image Repositories and Tags

<span style="color:red"><h6><b>Version: on-prem 3.x</b></h6></span>

<div>
<hr>
</div>

#### Prometheus

| REPOSITORY                                | TAG     |
| ----------------------------------------- | ------- |
| quay.io/prometheus/prometheus             | v2.25.2 |
| quay.io/coreos/prometheus-operator        | v0.32.0 |
| quay.io/coreos/prometheus-config-reloader | v0.32.0 |

#### Grafana

| REPOSITORY      | TAG   |
| --------------- | ----- |
| grafana/grafana | 7.4.3 |

#### Victoria Metrics

| REPOSITORY                       | TAG     |
| -------------------------------- | ------- |
| victoriametrics/victoria-metrics | v1.56.0 |

#### Anodot

| REPOSITORY                     | TAG   |
| ------------------------------ | ----- |
| anodot/prometheus-remote-write | 2.3.2 |
| anodot/anodot-kube-events      | 0.0.9 |
| anodot/pod-relabel-app         | 0.0.2 |

#### Kubernetes

| REPOSITORY                        | TAG    |
| --------------------------------- | ------ |
| quay.io/coreos/kube-state-metrics | v1.6.0 |

#### Exporters

| REPOSITORY                                                                            | TAG          |
| ------------------------------------------------------------------------------------- | ------------ |
| jmx-cassandra-exporter                                                                | **latest ?** |
| oliver006/redis_exporter                                                              | v1.11.1      |
| quay.io/prometheus/node-exporter                                                      | v1.0.1       |
| ssheehy/mongodb-exporter                                                              | 0.10.0       |
| justwatch/elasticsearch_exporter                                                      | 1.1.0        |
| 340481513670.dkr.ecr.us-east-1.amazonaws.com/anodot/repacked/danielqsj/kafka-exporter | **master**   |
| onprem-registry.anodot.local:5000/ssalaues/mongodb-exporter                           | 0.6.1        |
