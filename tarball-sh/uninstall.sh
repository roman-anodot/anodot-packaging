#!/bin/bash -x

resetCluster() {
        printf "\n ${YELLOW}`date +%H:%M` Reset k8s cluster... ${NC} \n\n"
	docker -H unix:///root/anodocker.socket exec anodot-kubespray /root/anodot-kubespray/installAnodot.sh reset
}

anodockerStop() {
    base=anodocker
    bridge=anodocker
	kill -9 $(cat /root/$base.pid)
    sleep 5
}

removeRPMs() {

	rpm -ehv docker-ce-18.09.9-3.el7.x86_64
	rpm -ehv docker-ce-cli-18.09.9-3.el7.x86_64
	rpm -ehv containerd.io-1.2.2-3.el7.x86_64
	rpm -ehv container-selinux-2.107-3.el7.noarch
}

removeDebs(){
    echo "remove deb packages"
}

anodockerRemove() {
    for i in `mount |grep overlay |awk '{ print $1}'`; do umount $i; done
    for i in `mount |grep shm |awk '{ print $1}'`; do umount $i; done
    for i in `mount |grep kubelet |awk '{ print $1}'`; do umount $i; done
    sleep 2
    for i in `mount |grep netns | awk '{ print $3 }'`; do
        umount "$i" 
    done

    #ip link delete name dev $bridge
    rm -rf /root/$base.data
    rm -rf /root/$base.exec
    rm -rf /root/$base.pid
    rm -rf /root/$base.socket
    rm -rf /root/*.log
}

removeArchive() {
    #cd /root/
    rm -rf anodotInstall anodotInstall.tar anodotInstall goAnodot.sh inventory-on-prem helm-values anodocker.data anodocker.exec anodocker-logfile.log
}

resetCluster
anodockerStop
removeRPMs
anodockerRemove
removeArchive