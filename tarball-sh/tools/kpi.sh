#!/usr/bin/env bash

customer_id=5eaaba942aa1b5000c2014d3

readonly RED='\033[0;31m'
readonly GREEN='\033[0;32m'
readonly ORANGE='\033[0;33m'
readonly BLUE='\033[0;34m'
readonly NC='\033[0m' # No Color

function print_subtitle {
#	printf "\n" 
#	printf "${1}${2}:${NC} "
	printf "${2}"
#	printf "%0.s-" {1..50}
}

function wip {
	printf "WIP\n"
}

print_subtitle $RED 'Alert Instances'
kubectl exec -it streamsets-agent-0 -c agent -- curl -X GET "elasticsearch-client:9200/alerts_$customer_id/_count" | cut -d : -f2 | awk -F',' '{print $1}'

print_subtitle $RED "Configured Alerts"
kubectl exec -it cassandra-0 -c cassandra -- cqlsh -e "select count(*) from events.configuration" | awk 'NR==4' 

print_subtitle $RED "Simulator Latency"
wip

print_subtitle $RED "Alert Bounces/Errors"
kubectl logs deploy/detection | egrep -i "\"error\"|bounce" | egrep -v "graphite|INFO|Hazelcast|Bootstrapper" | wc -l

print_subtitle $RED "Feedback"
wip

kubectl exec -it streamsets-agent-0 -c agent -- curl -X GET "elasticsearch-client:9200/_cat/indices" | grep $customer_id  >/tmp/aaa

print_subtitle $RED "Alert Index Size"
grep alert /tmp/aaa | grep open | awk ' {print $6}'

printf "\n"

print_subtitle $BLUE "Raw Anomalies (per rollup)"
printf "\n"
grep anomaly /tmp/aaa | awk -v cust="$customer_id" 'gsub(/^anomaly/, "", $3) gsub(cust, "", $3) gsub("_","",$3) {print "   "$3": "$6}'

print_subtitle $BLUE "Group Anomalies (per rollup)"
printf "\n"

print_subtitle $BLUE "Anomaly Index Size (per rollup)"
printf "\n"
grep anomaly /tmp/aaa | awk -v cust="$customer_id" 'gsub(/^anomaly/, "", $3) gsub(cust, "", $3) gsub("_","",$3) {print "   "$3": "$8}'

print_subtitle $BLUE "Score Histogram"
printf "Detection Dashboard\n"

print_subtitle $BLUE "Search Anomalies Latency"
printf "Detection Dashboard\n"

print_subtitle $BLUE "Anomalies Latency"
printf "Detection Dashboard\n"

print_subtitle $BLUE "Kafka Lag"
printf "\n"
kubectl exec -it kafka-0 -- /usr/bin/kafka-consumer-groups --describe --zookeeper kafka-zookeeper:2181 --group anodotd >/tmp/bbb 2>/dev/null
grep -v "^-" /tmp/bbb | grep anodotd | awk '{print "   "$1": "$5}'

print_subtitle $BLUE "Detection Failed Tasks"
printf "Detection Dashboard\n"

printf "\n"

print_subtitle $GREEN "EPS"
printf "Anodotd Dashboard\n"

print_subtitle $GREEN "Anodotd Queue Size"
wip

print_subtitle $GREEN "Metrics Indexed"
#grep metric /tmp/aaa | awk -v cust="$customer_id" 'gsub(/^metric/, "", $3) gsub(cust, "", $3) gsub("_","",$3) {print "   "$3": "$6}'
grep metric /tmp/aaa | grep open | awk '{print $6}'

print_subtitle $GREEN "Metrics Index Size"
#grep metric /tmp/aaa | awk -v cust="$customer_id" 'gsub(/^metric/, "", $3) gsub(cust, "", $3) gsub("_","",$3) {print "   "$3": "$8}'
grep metric /tmp/aaa | grep open | awk '{print $8}'

print_subtitle $GREEN "Metrics Cached"
wip

print_subtitle $GREEN "Cassandra Samples"
printf "\n"
column_family="short medium long longlong weekly"
> /tmp/ccc
for i in $column_family
do
	echo "Family: $i" >> /tmp/ccc
	kubectl exec -it cassandra-0 -c cassandra -- nodetool cfhistograms ${i}rollup rollup >> /tmp/ccc
done
egrep -m 1 "Percent|bytes" /tmp/ccc
egrep "Family|50%|99%|Max" /tmp/ccc
# kubectl exec -it cassandra-0 -c cassandra -- cqlsh -e "select * from system.sstable_activity"
# kubectl exec -it cassandra-0 -c cassandra -- nodetool cfstats -- system.sstable_activity
# kubectl exec -it cassandra-0 -c cassandra -- nodetool cfstats -- longrollup.rollup
# kubectl exec -it cassandra-0 -c cassandra -- nodetool cfhistograms longrollup rollup
# nodetool netstats,cfstats,cfhistograms
wip

print_subtitle $GREEN "Metrics Search Latency"
wip

print_subtitle $GREEN "Samples Fetch Latency"
wip

print_subtitle $GREEN "Samples Insert Latency"
wip

print_subtitle $GREEN "Concurrent Searches (thread pools? queues?)"
wip
#kubectl exec -it streamsets-agent-0 -c agent -- curl -X GET "elasticsearch-client:9200/_nodes/stats?metric=thread_pool&pretty"
kubectl exec -it streamsets-agent-0 -c agent -- curl -X GET "elasticsearch-client:9200/_cat/thread_pool?v"

printf "\n"

print_subtitle $ORANGE "Number of Composites (???)"
kubectl exec -it cassandra-0 -c cassandra -- cqlsh -e "select count(*) from events.configuration" | awk 'NR==4'

print_subtitle $ORANGE "Composite Sample Rate"
printf "Composite Dashboard\n"

print_subtitle $ORANGE "Composite Sample Latency"
printf "Composite Dashboard\n"

