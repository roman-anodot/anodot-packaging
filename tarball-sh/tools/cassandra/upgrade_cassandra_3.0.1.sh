action='upgrade_cassandra_3.0.1'
kubectl cp $action.cql cassandra-0:. -c cassandra
#kubectl exec cassandra-0 -c cassandra -- nodetool snapshot -kc flush.requests
kubectl exec cassandra-0 -c cassandra -- cqlsh -f $action.cql
kubectl exec cassandra-0 -c cassandra -- rm -f $action.cql
kubectl exec cassandra-0 -c cassandra -- cqlsh -e 'DESCRIBE SCHEMA' > $action.out 

validate=`egrep -c "bc_data|timezone|flushtimestart), reqflush" $action.out`

printf "Records: $validate\n"
