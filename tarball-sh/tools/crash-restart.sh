#!/bin/bash

echo "Pods restart sctipt started.. $(date)" >> deleted-pods.log
while true ; do
  pods_to_restart=$(kubectl get pod --no-headers | awk '$4 >= 5 { print $1 }')
  for pod_name in $pods_to_restart ; do
	  printf "%s" >> deleted-pods.log "$(date) - Deliting by CrashLoop:" && kubectl delete pod "$pod_name"  >> deleted-pods.log
  done
  sleep 10
done