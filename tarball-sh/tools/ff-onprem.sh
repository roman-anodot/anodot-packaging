#!/bin/bash

#
# Script for to configure default feature flags for onprem, after initial setup is completed and customer is created. 
#

# TODO 
# get local port for anodotd-webapp automatically
# get local IP automatically and use as var below in CURL request


# MAKE SURE 
# changed customers id
# ip and port of webapp 


curl -H 'Content-Type: application/json' -X PUT \
-d '{
  "customers": [
    {
      "id": "5ee0e27a255fc4000cd7c011",
      "ff": {
            "COMPOSITE_ENABLE_DELAY_AUTO_CALC": true,
            "ENABLE_FIXED_DATA_TTL": false,
            "ENABLE_INFLUENCING_NODATA": true,
            "ENABLE_FEEDBACK_IN_ALERT_TRIGGERS": true,
            "USE_MEDIAN_BIAS_CORRECTION": true,
            "CORRELATION_ENABLE_ALERT_CORRELATION": true,
            "COMPOSITE_CONVERT_EMA_HACK": true,
            "COMPOSITE_ENABLE_DELAY_WATERMARK": true,
            "META_METRIC_TAGS_ENABLE_EXTENTED_TOKEN_MATCH": true,
            "USE_PRIOR_AREA": true,
            "ENABLE_DELTA_DURATION": true,
            "ALERTS_ENABLE_ANOMALY_UPDATE": true,
            "AGGREGATION_ENABLE_SYNC_STATE": true,
            "ENABLE_BUCKET_START_TIME": true,
            "MIN_DELTA_OFFLINE_CALCULATION": true,
            "ENABLE_USER_MODEL_RESET_ON_REBALANCE": true,
            "ENABLE_USERS_GROUPS": true
        }
    }
  ],
  "global": {
            "COMPOSITE_ENABLE_DELAY_AUTO_CALC": true,
            "ENABLE_FIXED_DATA_TTL": false,
            "ENABLE_INFLUENCING_NODATA": true,
            "ENABLE_FEEDBACK_IN_ALERT_TRIGGERS": true,
            "USE_MEDIAN_BIAS_CORRECTION": true,
            "CORRELATION_ENABLE_ALERT_CORRELATION": true,
            "COMPOSITE_CONVERT_EMA_HACK": true,
            "COMPOSITE_ENABLE_DELAY_WATERMARK": true,
            "META_METRIC_TAGS_ENABLE_EXTENTED_TOKEN_MATCH": true,
            "USE_PRIOR_AREA": true,
            "ENABLE_DELTA_DURATION": true,
            "ALERTS_ENABLE_ANOMALY_UPDATE": true,
            "AGGREGATION_ENABLE_SYNC_STATE": true,
            "ENABLE_BUCKET_START_TIME": true,
            "MIN_DELTA_OFFLINE_CALCULATION": true,
            "ENABLE_USER_MODEL_RESET_ON_REBALANCE": true,
            "ENABLE_USERS_GROUPS": true
            }
}' 'http://10.0.0.13:32500/api/v1/token/configuration/feature?token=123456'


#
# request for check all current flags 
# curl 'http://10.0.0.112:32538/api/v1/token/configuration/feature/default?token=123456&userid=5ee0e27a255fc4000cd7c011' | jq
# 