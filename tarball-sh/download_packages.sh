### RHEL
pwd
mkdir -pv offline_repo_rhel && cd offline_repo_rhel
curl -O https://anodot-packages.s3.amazonaws.com/tarballs/offline_repo_rhel.tar
curl -O http://mirror.centos.org/centos/7/os/x86_64/Packages/socat-1.7.3.2-2.el7.x86_64.rpm
curl -O https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/readline-6.2-11.el7.x86_64.rpm
pwd
ls -la && cd -

### Ubuntu 16
mkdir -pv ubuntu16 && cd ubuntu16
curl -O https://anodot-packages.s3.amazonaws.com/tarballs/offline_repo_ubuntu_16.tar
cd -
pwd
ls -la

### Ubuntu 18
mkdir -pv ubuntu18 && cd ubuntu18
curl -O https://anodot-packages.s3.amazonaws.com/tarballs/offline_repo_ubuntu_18.tar
ls -la && cd - 
pwd
ls -la

### Prometheus Monitoring package.
mkdir -pv monitoring && cd monitoring
curl -O https://anodot-packages.s3.amazonaws.com/tarballs/prometheus_monitor.tar
ls -la
cd -
