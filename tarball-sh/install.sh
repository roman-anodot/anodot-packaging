#!/bin/bash

#set -e

source ./src/colors.sh
source ./src/utils

if [[ $EUID -ne 0 ]]; then
	 #printf "${RED} This script must be run as root ${NC} \n"
	 dashdebugfailed "This script must be run as root"
   exit 1
fi

if [ -f "./hosts.ini" ]; then
	echo "[5 seconds timout] check hosts.ini file: "
	cat ./hosts.ini
	echo ""
	sleep 5
else 
	dashdebugfailed "Check if hosts.ini prepared.. Exit." && exit 1
fi

cat ./src/welcome

usage() {
	cat ./src/usage
}

detectOS() {
	source ./src/detectos.sh
}

ANODOCKER_PATH="unix:///opt/anodocker/exec/anodocker.socket"
#ANODOT_KUBESPRAY_TAG=
#WEBREPO_TAG=
#REGISTRI_TAG=
SMTP_HOST=localhost
BRANCH=$(cat branch.txt)
echo "Branch: $BRANCH" && export BRANCH=$BRANCH

build_num() {
	cat ./build.txt
}

anodotUserCreation() {
	bash ./src/anodotuser.sh
}

getLocalIP() {
	./src/getIP.sh # it puts ip to the file below
	if [ -f "./src/getIP.sh" ]; then
		localIP=$(cat ./localIP.var)
	fi
	#### TODO: ADD IP CHECKS (Validation)
	if [ -z "$localIP" ]; then
		 echo "No IP, exit.." && exit 1
	else
		 return 0
	fi
}

if [[ -n $(command -v python) ]]; then
     PYTHON=$(command -v python)
		 #echo $PYTHON
elif [[ -n $(command -v python3) ]]; then
     PYTHON=$(command -v python3)
		 ln -s "${PYTHON}" /usr/bin/python
else
     echo "No python found.." && exit 1
fi

systemChecks() {
	source ./src/syscheck.sh
}

dockerDepsCheck() {
	echo "Docker dependencies - TBD"
}

dockerOfflineInstall() {
	./src/dockerinstall.sh
}

anodockerWrapper() {
	if [ ! -f "/usr/bin/anodocker" ]; then
		echo "docker -H ${ANODOCKER_PATH} \$@ " > /usr/bin/anodocker && chmod +x /usr/bin/anodocker
	fi
}

anodockerServiceStart() {
	echo "anodocker install dir: /opt/anodocker"
	if [ ! -d "/opt/anodocker" ]; then
		mkdir -pv /opt/anodocker/{data,exec}
	fi
	if [ -d "/etc/systemd/system/" ]; then
		cp -v ./src/anodocker.service /etc/systemd/system/anodocker.service
	fi
	systemctl daemon-reload
	systemctl start anodocker.service
	systemctl enable anodocker.service
	systemctl status anodocker.service
	#
	#./src/anodocker.sh start
	#
}

dockerLoadImages() {
	source ./src/imagesload.sh
}

registryRun() {
	./src/registry-run.sh
}

webrepoRun() {
	./src/webrepo-run.sh
}

anodotSprayRun() {
	./src/spray-run.sh $SMTP_HOST
}

smtpContainerRun() {
	./src/run-smtp-container.sh
}

imagesLoaded() {
	printf "\n${YELLOW}Images loaded, installation continue in 5 sec., or proceed to manual installation by Ctrl+C and login to anodot-kubespray..${NC}\n"
	sleep 5
	#read -p "Press Enter to continue auto installation"
}

copyConfigFiles() {
	./src/copy_config_files.sh
}

k8sInstall() {
	# TODO add Anodot installation PATH
	printf "\n${YELLOW}`date +%H:%M:%S` Start k8s installation on $(cat ./localIP.var) ${NC} \n"
	anodocker exec anodot-kubespray /root/anodot-kubespray/installAnodot.sh "k8s" "$(cat ./localIP.var)"
}

anodotInstall() {
	#read -r -p " " response
	read -t 5 -r -p "Continue and install Anodot automatically without resources/config changes? Will continue installation in 5 seconds.. [Y/n] " response
	if [ -z "$response" ]; then
		response=Y
		echo "Yes by default.."
	fi

	case "$response" in
		[yY][eE][sS]|[yY]) 
			printf "\n${YELLOW}`date +%H:%M:%S` Install Anodot HELMs... ${NC} \n"
			anodocker exec  anodot-kubespray /root/anodot-kubespray/installAnodot.sh "anodot"
			;;
		*)
			printf "\nOk, proceed to manual installation from spray: anodocker exec -it anodot-kubespray bash" && exit 1
			;;
	esac
}

configElasticAccess() {
	printf "\n${YELLOW}`date +%H:%M:%S` Elastic access configuration.. ${NC} \n"
	anodocker exec  anodot-kubespray /root/anodot-kubespray/createElasticAccess.sh
}

#### List of function to perform
commands=(
build_num
anodotUserCreation
getLocalIP
systemChecks
aptTimeoutChange
dockerDepsCheck
dockerOfflineInstall
anodockerWrapper
anodockerServiceStart
dockerLoadImages
registryRun
webrepoRun
anodotSprayRun
imagesLoaded
copyConfigFiles
k8sInstall
anodotInstall
smtpContainerRun
)

installLog="install.log"
if [ ! -f "$installLog" ]; then
	touch $installLog
fi
for function in "${commands[@]}"; do
	eval "${function}" "$@" | tee -a $installLog
	exit_status=${PIPESTATUS[0]}
	if [[ $exit_status != 0 ]]; then
		printf "${RED}FAILED $(date +%H:%M:%S) ${function} ${NC}\n" | tee -a $installLog
		exit 1
	else
		printf "${GREEN}OK $(date +%H:%M:%S) ${function} ${NC}\n" | tee -a $installLog
	fi
done
printf "${GREEN}\n"
cat ./src/complete
printf "${NC}"

printf "Check webapp: http://%s/\n" "$(cat ./localIP.var)"
