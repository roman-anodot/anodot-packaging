INSTALL:
Currently script support one host installation only.

- Do untar from regular user of archive - anodotInstall.tar:
	> mkdir anodotInstall && tar -xvf anodotInstall.tar -C anodotInstall
- Go to anodotInstall folder and run install.sh script with sudo (or under root);
	> cd anodotInstall
	> sudo su -
	> ./install.sh
- Follow by script processes. In some steps, script will ask to continue by press Enter;
- In case of issues contact: devops-kiev@anodot.com

SCALE:
- in anodot-kubespray
- update hosts.ini file with new nodes;
- run scale ansible command: 
	> ansible-playbook -v -e DOCKER_REGISTRY_IP=registryIP docker-registry.yaml scale.yml kubectl-config.yaml pv-config.yaml

RESET:
- in anodot-kubespray
- if cluster should be completely removed:
	> ansible-playbook -v reset.yml

SMTP configuration:
- 