#!/bin/bash 
source ./src/colors.sh
source ./src/utils
source ./src/detectos.sh

if commandExists "docker"; then
    #return
    echo "docker already installed." && docker --version
else
    dashdebugwarn "No Docker installed, continue to offline installation, or Ctrl+C for cancel."
    #read -p "Press Enter to continue or Ctrl+C for cancel"
    ./src/timer.sh
    if [ -n "$ostype" ]; then
        printf "Detected distribution is ${ostype} $version \n"
        ostype="$(echo "$ostype" | tr '[:upper:]' '[:lower:]')"
        case "$ostype" in
            ubuntu)
                if [ $version = "16.04" ]; then
                    DOCKER_FOLDER="/opt/docker"
                    ANODOT_FOLDER="/opt/anodot"
                    mkdir -pv ${DOCKER_FOLDER}
                    mkdir -pv ${ANODOT_FOLDER}
                    cd ./ubuntu16 && tar -xvf offline_repo_ubuntu_16.tar && cd -
                    dpkg -i ./ubuntu16/*.deb
                elif [ $version = "18.04" ]; then
                    DOCKER_FOLDER="/opt/docker"
                    ANODOT_FOLDER="/opt/anodot"
                    mkdir -pv ${DOCKER_FOLDER}
                    mkdir -pv ${ANODOT_FOLDER}
                    cd ./ubuntu18 && tar -xvf offline_repo_ubuntu_18.tar && cd -
                    dpkg -i ./ubuntu18/*.deb
                else
                     echo "not recognized Ubuntu version..." && exit 1
                fi
                ;;
            debian)
                echo "TBD"
                ;;
            fedora)
                echo "TBD"
                ;;
            rhel)
                #### folders used for Eti - TODO: Change to some default folder for all installs.
                DOCKER_FOLDER="/app1/docker"
	            ANODOT_FOLDER="/app1/anodot"
	            mkdir -pv ${DOCKER_FOLDER}
	            mkdir -pv ${ANODOT_FOLDER}
                cd ./offline_repo_rhel && tar -xvf offline_repo_rhel.tar && cd -
                #rpm -Uvh --nodeps --replacefiles --replacepkgs ./offline_repo_rhel/*.rpm
                cd ./offline_repo_rhel/
                rpm -Uvh --nodeps --replacefiles --replacepkgs python-IPy-0.75-6.el7.noarch.rpm && mv python-IPy-0.75-6.el7.noarch.rpm python-IPy-0.75-6.el7.noarch.bak
                for i in $(ls -1 | grep rpm) ; do rpm -Uvh --nodeps --replacefiles --replacepkgs $i; done
                cd -
                ;;
            centos)
                DOCKER_FOLDER="/data1/docker"
	            ANODOT_FOLDER="/data1/anodot"
	            mkdir -pv ${DOCKER_FOLDER}
	            mkdir -pv ${ANODOT_FOLDER}
                cd ./offline_repo_rhel && tar -xvf offline_repo_rhel.tar && cd -
                #rpm -Uvh --nodeps --replacefiles --replacepkgs ./offline_repo_rhel/*.rpm
                cd ./offline_repo_rhel/
                rpm -Uvh --nodeps --replacefiles --replacepkgs python-IPy-0.75-6.el7.noarch.rpm && mv python-IPy-0.75-6.el7.noarch.rpm python-IPy-0.75-6.el7.noarch.bak
                for i in $(ls -1 | grep rpm) ; do rpm -Uvh --nodeps --replacefiles --replacepkgs $i; done
                cd -
                ;;
            *)
                printf "That is an unsupported distribution. \n\n" && exit 1
                ;;
        esac
    fi
    if commandExists "docker"; then
        ##### enable and start docker service
        systemctl enable docker.service
        systemctl start docker.service
        echo "System docker version:"
        docker info | grep -A1 -B1 -e "Server Version" -e "Total Memory"
        #dashdebugok" Docker enabled and started."
    else
        echo "OS not detected."
        printf "${RED}FAILED `date +%H:%M:%S` Docker not installed properly. ${NC}\n"
        exit 1
    fi
fi
