#!/bin/bash
source ./src/colors.sh
BRANCH=${BRANCH}
printf "${YELLOW}$(date +%H:%M) Run webrepo... ${NC} \n"
if ! anodocker ps -a |grep -q webrepo ; then
		# Run anodot webrepo
		anodocker run -d  \
		--name webrepo  \
		--restart=always \
		-p 8081:8081 \
		340481513670.dkr.ecr.us-east-1.amazonaws.com/anodot/webrepo:"${BRANCH}"
else
	anodocker start webrepo
fi
