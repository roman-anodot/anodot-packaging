source ./src/colors.sh

	printf "${YELLOW}$(date +%H:%M) Loading images..: ${NC}\n"
    
	if anodocker ps |grep -q anodot-kubespray ; then
		 printf "kubespray loaded already\n"
	else
		printf "${YELLOW}$(date +%H:%M) Loading anodot-kubespray... ${NC} \n"
		anodocker load -i ./images/spray-onprem-tar.tgz
	fi
	if [ ! -z "`anodocker ps |grep webrepo`" ]; then
		 printf "webrepo loaded already\n"
	else
		printf "${YELLOW}$(date +%H:%M) Loading webrepo... ${NC} \n"
		anodocker load -i ./images/webrepo-onprem-tar.tgz
	fi
	if [ ! -z "`anodocker ps |grep registry`" ]; then
		printf "registry loaded already\n"
	else
		printf "${YELLOW}$(date +%H:%M) Loading registry... ${NC} \n"
		anodocker load -i ./images/registry-onprem-tar.tgz
	fi
