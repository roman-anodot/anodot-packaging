# currently used for one host cluster..
localIP=$1

cat >/root/anodot-kubespray/inventory/anodot-onprem/hosts.ini <<EOF
[k8s-cluster:children]
kube-master
kube-node
[all]
${localIP} etcd_member_name=${localIP} ansible_user=ec2-user ansible_become=yes ansible_ssh_private_key_file=/root/.ssh/k8s-staging.pem ansible_python_interpreter=/usr/bin/python
[kube-master]
${localIP}
[kube-node]
${localIP}
[etcd]
${localIP}
EOF