#!/bin/bash

osType() {
	if [ -f "/etc/redhat-release" ]; then
		#cat /etc/redhat-release
		ostype="$(. /etc/os-release && echo "$ID")"
	elif [ -f "/etc/os-release" ]; then
		#cat /etc/os-release
		ostype="$(. /etc/os-release && echo "$ID")"
	elif [ -f "cat /etc/centos-release" ]; then
		#cat /etc/centos-release
		ostype="$(. /etc/os-release && echo "$ID")"
	else
		echo "versions not found, OS may be not supported..."
	fi
}

if [ -z "`cat /etc/passwd|grep anodot`" ]; then
	useradd -m -s /bin/bash anodot
	osType
	if [ $ostype = "ubuntu" ]; then
		echo "anodot:anodot" | chpasswd
	elif [ $ostype = "centos" ]; then
		echo anodot | passwd --stdin anodot
	elif [ $ostype = "rhel" ]; then
		echo anodot | passwd --stdin anodot
	else
		return
	fi
	mkdir -pv /home/anodot/.ssh/
	touch /home/anodot/.ssh/authorized_keys
	# !careful here, sudo may be broken!
	echo 'anodot ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers
	#ssh-keygen -t rsa -N "" -f /root/.ssh/anodot.key
	ssh-keygen -t rsa -N "" -f ${HOME}/.ssh/anodot.key
	# TODO check it
	echo "Current home working directory: ${HOME}"
	# CHeck how to make keys properly for any user like ubuntu,ec2-user, and root etc.
	chmod 400 "${HOME}"/.ssh/anodot.key "${HOME}"/.ssh/anodot.key.pub
	cat "${HOME}"/.ssh/anodot.key.pub >> /home/anodot/.ssh/authorized_keys
else
		cat /etc/passwd|grep anodot && echo "anodot user exists"
fi
