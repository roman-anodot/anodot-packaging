SMTP() {
	printf "\n ${LIGHT_BLUE}Provide smtp: ${NC}\n"
	read SMTP_HOST
	echo "SMTP host: $SMTP_HOST"
}

sendTestEmail() {
[[ $@ ]] || {
    printf "Usage\n\t./$0 $SMTP_HOST <email> <from_email> <rcpt_email>\n"
    exit 1
}
{
    sleep 1
    echo "helo $2"
    sleep 0.5
    echo "mail from:$3"
    sleep 0.5
    echo "rcpt to:$4"
    echo
} | telnet $1 25 |
    grep -q "Unknown user" &&
    echo "Invalid email" ||
    echo "Valid email"
}

checkSmtpConneection() {
if nc -w2 $SMTP_HOST 25 
then 
    echo smtp connection worked
else 
    echo smtp connection failed
fi
}
