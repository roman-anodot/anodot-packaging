#!/bin/bash

GREEN='\033[0;32m'
BLUE='\033[0;94m'
LIGHT_BLUE='\033[0;34m'
YELLOW='\033[0;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color


function Colors()
{
	Escape="\033";

	BlackF="${Escape}[30m"; RedF="${Escape}[31m"; GreenF="${Escape}[32m";
	YellowF="${Escape}[33m"; BlueF="${Escape}[34m"; Purplef="${Escape}[35m";
	CyanF="${Escape}[36m"; WhiteF="${Escape}[37m";

	BlackB="${Escape}[40m"; RedB="${Escape}[41m"; GreenB="${Escape}[42m";
	YellowB="${Escape}[43m"; BlueB="${Escape}[44m"; PurpleB="${Escape}[45m";
	CyanB="${Escape}[46m"; WhiteB="${Escape}[47m";

	BoldOn="${Escape}[1m"; BoldOff="${Escape}[22m";
	ItalicsOn="${Escape}[3m"; ItalicsOff="${Escape}[23m";
	UnderlineOn="${Escape}[4m"; UnderlineOff="${Escape}[24m";
	BlinkOn="${Escape}[5m"; BlinkOff="${Escape}[25m";
	InvertOn="${Escape}[7m"; InvertOff="${Escape}[27m";

	Reset="${Escape}[0m";
}

function dashdebugok()
{
	Colors
	for ((x = 0; x < ${#1}+10; x++)); do
	  printf "%s" "-"
	done
	 echo ""
	 echo -e "`date +%H:%M:%S` ${GreenF}[OK]${Reset}  $1"

	 for ((x = 0; x < ${#1}+10; x++)); do
	  printf "%s" "-"
	done
	echo ""
}

function dashdebugfailed()
{
	Colors
	for ((x = 0; x < ${#1}+12; x++)); do
	  printf "%s" "-"
	done
	 echo ""
	 echo -e "`date +%H:%M:%S` ${RedF}[FAILED]${Reset}  $1"

	 for ((x = 0; x < ${#1}+12; x++)); do
	  printf "%s" "-"
	done
	echo ""
}

function dashdebugwarn()
{
	Colors
	for ((x = 0; x < ${#1}+12; x++)); do
	  printf "%s" "-"
	done
	 echo ""
	 echo -e "`date +%H:%M:%S` ${Purplef}[WARN]${Reset}  $1"

	 for ((x = 0; x < ${#1}+12; x++)); do
	  printf "%s" "-"
	done
	echo ""
}

function dashdebuginfo()
{
	Colors
	for ((x = 0; x < ${#1}+12; x++)); do
	  printf "%s" "-"
	done
	 echo ""
	 echo -e "`date +%H:%M:%S` ${YellowF}[INFO]${Reset}  $1"

	 for ((x = 0; x < ${#1}+12; x++)); do
	  printf "%s" "-"
	done
	echo ""
}

function debugok()
{
	Colors
	 echo -e "`date +%H:%M:%S` ${GreenF}[OK]${Reset}  $1"
}

function debugwarn()
{
	Colors
	 echo -e "`date +%H:%M:%S` ${Purplef}[WARN]${Reset}  $1"
}