#!/bin/bash -x

POD=$(kubectl get pod -l run=nodejsautomation -o jsonpath="{.items[0].metadata.name}")
#kubectl exec $POD -- [ -s /usr/src/app/reports/system_tests_report_1.xml ] && echo "File not empty" || echo "File empty"

#kubectl exec $POD -- [ -s /usr/src/app/reports/system_tests_report_1.xml ]
until kubectl exec $POD -- test -s /usr/src/app/reports/system_tests_report_1.xml
do
    echo "Check again later..."
    sleep 3
done

echo "Downloading report"

kubectl cp "$POD":/usr/src/app/reports/ .

# kubectl logs $POD > nodejs-logs.txt
# scp -i ~/.ssh/k8s-staging.pem ubuntu@3.87.246.146:/home/ubuntu/nodejs-logs.txt .
# scp -i ~/.ssh/k8s-staging.pem ubuntu@3.87.246.146:/home/ubuntu/system_tests_report_1.xml .