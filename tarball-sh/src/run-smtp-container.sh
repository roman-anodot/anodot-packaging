#!/bin/bash

#### check if it's in local registry:
# curl -X GET http://onprem-registry.anodot.local:5000/v2/namshi/smtp/tags/list

#
echo ""
read -t 5 -r -p "(Required for DEV/TEST only) Install SMTP Container to this VM - $(hostname)? Will continue installation in 5 seconds.. [Y/n] " response
if [ -z "$response" ]; then
    response=Y
    echo "Yes by default.."
fi

case "$response" in
    [yY][eE][sS]|[yY]) 
        printf "Run smtp container to the system's docker.."
        docker run -d --name=smtp \
        --restart always \
        --network host \
        -e RELAY_DOMAINS="anodot.com : gmail.com" \
        onprem-registry.anodot.local:5000/namshi/smtp:latest
        ;;
    *)
        echo "Ok, continue without SMTP container.."
        echo ""
        ;;
esac
