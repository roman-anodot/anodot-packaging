#!/bin/bash
for (( i=10; i>0; i--)); do
    printf "\rContinue installation in $i seconds.  Hit any key to continue.."
    read -s -n 1 -t 1 key
    if [ $? -eq 0 ]
    then
        break
    fi
done
echo "Resume installation..."
