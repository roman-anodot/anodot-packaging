#!/bin/bash

source ./src/colors.sh
source ./src/utils
dashdebugwarn "The next system params will be checked:"
cat << EOF
		Disks - size, etc..
		Mem - requirements, free..
		Swap on
		Network - requesremetns.
		OS - supported OS..
		YUM/APT repo connections...
		SELinux - enabled/disabled
		FirewWall - enabled/disabled
		Docker - installed/not installed, version..
		Python - exists, version..
		Socat (required software for internal k8s port forwards) dependency for k8s..
		SMTP - IP/hostname, connection check..
EOF
printf "\n"
### Checks
disks() {
	echo "Disks:"
	df -h
}
memory() {
	echo "Memory:"
	free -m
}
swapcheck() {
	echo "List file: /proc/swaps"
	cat /proc/swaps
}
# Networks show excluded.
network() {
	echo "Networking:"
	if [ `command -v ifconfig` ]; then
		ifconfig
	else
		ip a
	fi
}
os(){
	source ./src/detectos.sh
	echo "OS: $ostype"
	echo "Version: $version"
}
checkSE() {
	osType
	if [ $ostype = "ubuntu" ]; then
		 echo "no SE in ubuntu"  && return 1
	elif [ $ostype = "centos" ]; then
		echo "Check SE Linux"
		 getenforce
	elif [ $ostype = "rhel" ]; then
		echo "Check SE Linux"
		 getenforce
	else
		 "can't check SE"  && return 1
	fi
}
fwStatus() {
	if commandExists "firewalld"; then
        systemctl status firewalld
	else
		echo "No firewalld installed"  && return 1
    fi
}
pythonVersion() {
	echo "Python check:"
	if commandExists "python"; then
        python -V
	elif commandExists "python3"; then
		python3 -V
	else 
		echo "python not found"  && return 1
    fi
}
# TODO: Move it to dependecies installation script when it is ready..
socatVersion() {
	if commandExists "socat"; then
		socat -V | grep version
	else
		printf "\nNo Socat installed - required for HELM, if docker installation done online, also do installation for socat manually.\n Press enter to continue.."
		#read -p ""
		./src/timer.sh
		return 1
    fi
}
mailCommandCheck() {
	if commandExists "mail"; then
        echo "mail command exists"
	else
		echo "no mail installed"  && return 1
    fi
}
smtpConnectionCheck() {
	echo "smtp TBD"  && return 1
}
#### List of function to perform
checks=(
os
disks
memory
swapcheck
checkSE
fwStatus
pythonVersion
socatVersion
mailCommandCheck
smtpConnectionCheck
)

if [ ! -f "sysreport.txt" ]; then
	touch sysreport.txt
fi
for check in "${checks[@]}"; do
	eval "${check}" "$@" | tee -a sysreport.txt
	exit_status=${PIPESTATUS[0]}
    if [[ $exit_status != 0 ]]; then
			printf "${YELLOW}WARN `date +%H:%M:%S` ${check} ${NC}\n" | tee -a sysreport.txt
		else
			printf "${GREEN}OK `date +%H:%M:%S` ${check} ${NC}\n" | tee -a sysreport.txt
		fi
done
dashdebugwarn "$(date +%H:%M:%S) Review system checks and confirm installation start..."
#read -p "Press enter to continue"
./src/timer.sh
printf "\n"
