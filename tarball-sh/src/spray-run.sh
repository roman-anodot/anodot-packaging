source ./src/colors.sh
	BRANCH=${BRANCH}
    printf "${YELLOW}$(date +%H:%M) Run anodot-kubespray... ${NC} \n"
	localIP=$(cat ./localIP.var)
	SMTP_HOST=$1
	if ! anodocker ps -a |grep -q anodot-kubespray ; then
		anodocker run -t -d                           \
		--restart=always \
    	--hostname anodot-kubespray                                                 \
    	--name anodot-kubespray                                                     \
    	--volume="${HOME}"/inventory-on-prem:/root/anodot-kubespray/inventory/on-prem \
    	--volume="${HOME}"/.kube/:/root/.kube/                                        \
    	--volume="${HOME}"/.ssh/:/root/.ssh/                                          \
    	--volume="${HOME}"/.spray_history:/root/.bash_history                         \
    	-e "localIP=${localIP}"                                                     \
		-e "SMTP_HOST=${SMTP_HOST}"                                                 \
   	 	340481513670.dkr.ecr.us-east-1.amazonaws.com/anodot-kubespray:"${BRANCH}" bash
	else
		 anodocker start anodot-kubespray
	fi

