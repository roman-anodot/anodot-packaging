source ./src/colors.sh
if [ `command -v ip` ]; then
        printf "\n---Getting local IPs---\n"
	printf "Primary IP: "
	hostname --ip-address
	printf "\nList of all IPs found: \n"
	for i in `ip -o link show|awk -F': ' '{print $2}'`; do
		printf "$i "
		ip a show $i | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*'
	done
	printf "\n${YELLOW}Check IPs and choose to which k8s should be installed (10 seconds timeout)..:${NC}"
	read -t 10 localIP
	if [ ! -z "$localIP" ]; then
                printf "IP for installation will be used: $localIP \n"
                echo "$localIP" > localIP.var
		#read -p "Press Enter to continue..."
		./src/timer.sh
        else
			printf "\n Taking default IP with cmd: 'hostname --ip-address' - $(hostname --ip-address) \n"
			localIP=$(hostname --ip-address)
			echo "$localIP" > localIP.var
        fi
fi
