#!/bin/bash
# TBD
while getopts "R:T:E:C:F:K:S:h" OPTION
do
    case $OPTION in
        R) REPORT=$OPTARG;;
        T) TAG=$OPTARG;;
        E) environment=$OPTARG;;
        B) bastionIP=$OPTARG;;
        C) clusterIPs=$OPTARG;;
        F) fromaddress=$OPTARG;;
        S) SMTP_HOST=$OPTARG;;
        K) sshkey=$OPTARG;;
        h) usage; exit 1;;
        *) usage; exit 1;;
    esac
done
