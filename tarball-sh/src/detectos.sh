#!/bin/bash

if [ -f /etc/centos-release ] && [ -r /etc/centos-release ]; then
    # CentOS 6 example: CentOS release 6.9 (Final)
    # CentOS 7 example: CentOS Linux release 7.5.1804 (Core)
    ostype="$(cat /etc/centos-release | cut -d" " -f1)"
    version="$(cat /etc/centos-release | sed 's/Linux //' | cut -d" " -f3 | cut -d "." -f1-2)"
elif [ -f /etc/os-release ] && [ -r /etc/os-release ]; then
    ostype="$(. /etc/os-release && echo "$ID")"
    version="$(. /etc/os-release && echo "$VERSION_ID")"
elif [ -f /etc/redhat-release ] && [ -r /etc/redhat-release ]; then
    # this is for RHEL6
    ostype="rhel"
    major_version=$(cat /etc/redhat-release | cut -d" " -f7 | cut -d "." -f1)
    minor_version=$(cat /etc/redhat-release | cut -d" " -f7 | cut -d "." -f2)
    version=$major_version
elif [ -f /etc/system-release ] && [ -r /etc/system-release ]; then
    if grep --quiet "Amazon Linux" /etc/system-release; then
        # Special case for Amazon 2014.03
        dist="amzn"
        version=`awk '/Amazon Linux/{print $NF}' /etc/system-release`
    fi
else
    printf "OS type cannot be determined because no version files exist."
    exit 1
fi
